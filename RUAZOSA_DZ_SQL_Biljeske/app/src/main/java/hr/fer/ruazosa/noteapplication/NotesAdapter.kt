package hr.fer.ruazosa.noteapplication
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class NotesAdapter(listOfNotesViewModel: NotesViewModel, itemClickListener: OnItemClickListener): RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    var listOfNotes: NotesViewModel = listOfNotesViewModel
    val itemClickListener: OnItemClickListener = itemClickListener

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var noteTitleTextView: TextView? = null
        var noteDateTextView: TextView? = null
        //referenca na delete gumb
        var deleteFloatingActionButton: FloatingActionButton? = null

        init {
            noteTitleTextView = itemView.findViewById(R.id.noteTitleTextView)
            noteDateTextView = itemView.findViewById(R.id.noteDateTextView)
            deleteFloatingActionButton = itemView.findViewById(R.id.deleteFloatingActionButton)
        }

        fun bind(notes: NotesViewModel, clickListener: OnItemClickListener, position:Int, adapter: NotesAdapter)
        {
            var noteEntity: NoteEntity = notes.getNoteEntityAtPosition(position)
            noteTitleTextView?.text = noteEntity.noteTitle
            noteDateTextView?.text = noteEntity.noteDate.toString()

            // definiraj akciju prilikom pritiska delete gumba
            deleteFloatingActionButton?.setOnClickListener{
                //Log.d("Delete_Debug", "Delete button was pressed!")
                notes.deleteNoteFromRepository(noteEntity)
                // bez ovoga se promjena ne vidi u MainActivity, problem s observerom
                // tj. vjerojatno neispravna implementacija s moje strane
                adapter.notifyDataSetChanged()
                notes.getNotesRepository()
                // ili mozda ovako?
                //notes.getNotesRepository()
            }

            itemView.setOnClickListener {
                clickListener.onItemClicked(noteEntity)
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NotesAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val notesListElement = inflater.inflate(R.layout.note_list_element, parent, false)
        return ViewHolder(notesListElement)
    }

    override fun getItemCount(): Int {
        return listOfNotes.getNotesRepositorySize()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (listOfNotes != null) {
            //val noteEntity = listOfNotes.getNoteEntityAtPosition(position)
            viewHolder.bind(listOfNotes, itemClickListener, position, this)

            /*viewHolder.noteTitleTextView?.text = listOfNotes.notesList.value!![position].noteTitle

            viewHolder.noteDateTextView?.text =
                listOfNotes.notesList.value!![position].noteDate.toString()

            // definiraj akciju prilikom pritiska delete gumba
            viewHolder.deleteFlotaingActionButton?.setOnClickListener{
                Log.d("Delete_Debug", "Delete button was pressed!")
            }*/
        }
    }
}

interface OnItemClickListener{
    fun onItemClicked(noteEntity: NoteEntity)
}