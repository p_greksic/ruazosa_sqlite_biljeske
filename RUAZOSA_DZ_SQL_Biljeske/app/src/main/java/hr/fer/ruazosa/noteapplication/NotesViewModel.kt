package hr.fer.ruazosa.noteapplication

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class NotesViewModel(application: Application): AndroidViewModel(application) {
    private val notesRepository: NotesRepository
    var notesList = MutableLiveData<List<NoteEntity>>()
        private set

    init {
        notesRepository = NotesRepository(application)
        notesList.value = notesRepository.getNoteEntitiesFromRepository()
    }
    fun getNotesRepository() {
        notesList.value = notesRepository.getNoteEntitiesFromRepository()
    }

    fun getNotesRepositorySize() : Int {
        if(notesList.value != null){
            return notesList.value!!.size
        }else{
            return 0
        }
    }

    fun getNoteEntityAtPosition(position: Int) : NoteEntity{
        return notesList.value!![position]
    }

    fun saveNoteToRepository(noteEntity: NoteEntity) {
        notesRepository.saveNoteToRepository(noteEntity)
    }

    fun deleteNoteFromRepository(noteEntity: NoteEntity){
        notesRepository.deleteNoteEntityFromRepository(noteEntity)
    }

    fun updateNoteFromRepository(editedNoteEntity: NoteEntity){
        notesRepository.updateNoteEntityFromRepository(editedNoteEntity)
    }

    fun closeNotesRepository(){
        notesRepository.closeDatabase()
    }
}