package hr.fer.ruazosa.noteapplication

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.Date

@Entity
data class NoteEntity(
    @PrimaryKey(autoGenerate = true) var noteId: Int,
    @ColumnInfo(name = "noteTitle") var noteTitle: String?,
    @ColumnInfo(name = "noteDescription") var noteDescription: String?,
    @ColumnInfo(name = "noteDate") var noteDate: Date?
)