package hr.fer.ruazosa.noteapplication

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

@Database(entities = arrayOf(NoteEntity::class), version = 1)
@TypeConverters(Converters::class)
abstract class NotesDatabase : RoomDatabase() {
    abstract fun notesDao(): NotesDao

    companion object{

        @Volatile
        private var databaseInstance: NotesDatabase? = null

        fun getDatabase(context: Context): NotesDatabase? {
            if (databaseInstance == null) {
                synchronized(NotesDatabase::class.java) {
                    if (databaseInstance == null) {
                        databaseInstance = Room.databaseBuilder(
                            context.applicationContext,
                            NotesDatabase::class.java, "notes_database"
                        ).build()
                    }
                }
            }
            return databaseInstance as NotesDatabase
        }

        fun closeDatabase(){
            databaseInstance?.close()
        }
    }
}