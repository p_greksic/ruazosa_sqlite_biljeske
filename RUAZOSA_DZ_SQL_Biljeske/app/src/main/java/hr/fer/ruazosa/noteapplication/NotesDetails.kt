package hr.fer.ruazosa.noteapplication

import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_notes_details.*
import java.util.*

class NotesDetails : AppCompatActivity() {
    private val EDIT_NOTE_ACTIVITY_REQUEST_CODE = 1
    private val NEW_NOTE_ACTIVITY_REQUEST_CODE = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_details)
        val extras = intent.extras
        if(extras != null){
            // provjeri request code
            if(extras.getInt("Request_Code") == EDIT_NOTE_ACTIVITY_REQUEST_CODE){
                noteTitleEditText.text = Editable.Factory.getInstance().newEditable(extras.getString("Note_Title"))
                noteDescriptionEditText.text = Editable.Factory.getInstance().newEditable(extras.getString("Note_Description"))
                saveNoteButton.text = getString(R.string.update_note_button_string)

                saveNoteButton.setOnClickListener {
                    // postavi povratne vrijednosti za MainActivity
                    val intent = Intent()
                    intent.putExtra("Request_Code", EDIT_NOTE_ACTIVITY_REQUEST_CODE)
                    intent.putExtra("Note_Title", noteTitleEditText.text.toString())
                    intent.putExtra("Note_Description", noteDescriptionEditText.text.toString())
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }else if(extras.getInt("Request_Code") == NEW_NOTE_ACTIVITY_REQUEST_CODE){
                saveNoteButton.setOnClickListener {
                    // postavi povratne vrijednosti za MainActivity
                    val intent = Intent()
                    intent.putExtra("Request_Code", EDIT_NOTE_ACTIVITY_REQUEST_CODE)
                    intent.putExtra("Note_Title", noteTitleEditText.text.toString())
                    intent.putExtra("Note_Description", noteDescriptionEditText.text.toString())
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
        }else {
            //should not happen
            finish()
        }
    }


}
