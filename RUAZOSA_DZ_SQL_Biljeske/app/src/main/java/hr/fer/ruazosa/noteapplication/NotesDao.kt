package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface NotesDao {
    @Query("SELECT * FROM noteEntity")
    fun getAll(): List<NoteEntity>

    @Query("SELECT * FROM noteEntity WHERE noteId IN (:noteIds)")
    fun loadAllNotesByIds(noteIds: IntArray): List<NoteEntity>

    @Query("SELECT * FROM noteEntity WHERE noteId=:noteId")
    fun loadNoteById(noteId: Int): List<NoteEntity>

    @Query("SELECT * FROM noteEntity WHERE noteTitle LIKE :title LIMIT 1")
    fun findNoteByTitle(title: String): NoteEntity

    @Query("SELECT * FROM noteEntity WHERE noteDescription LIKE :description LIMIT 1")
    fun findNoteByDescription(description: String): NoteEntity

    @Query("SELECT * FROM noteEntity WHERE noteDescription LIKE :description AND noteTitle LIKE :title LIMIT 1")
    fun findNoteByTitleAndDescription(title: String, description: String): NoteEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(noteEntities: List<NoteEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(noteEntity: NoteEntity)

    @Delete
    fun deleteNote(noteEntity: NoteEntity)

    @Update
    fun updateNote(noteEntity: NoteEntity)

    @Query("SELECT * FROM noteEntity WHERE noteId = (SELECT max(noteId) FROM noteEntity)")
    fun getLastSavedNote(): NoteEntity
}