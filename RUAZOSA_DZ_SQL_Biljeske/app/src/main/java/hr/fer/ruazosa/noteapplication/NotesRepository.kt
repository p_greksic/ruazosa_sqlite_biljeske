package hr.fer.ruazosa.noteapplication

import android.app.Application


class NotesRepository(application: Application){
    private var notesDatabase: NotesDatabase? = null
    private var notesDao: NotesDao? = null

    init{
        notesDatabase = NotesDatabase.getDatabase(application)
        notesDao = notesDatabase?.notesDao()
    }


    fun getNoteEntitiesFromRepository(): List<NoteEntity>? {
        var noteEntities: List<NoteEntity>? = null

        var getAllThread = Thread(Runnable {
            noteEntities = notesDao!!.getAll()
        })

        getAllThread.start()
        getAllThread.join()
        return noteEntities
    }

     fun updateNoteEntityFromRepository(noteEntity: NoteEntity){
        Thread( Runnable {
            notesDao!!.updateNote(noteEntity)
        }).start()
    }

    fun deleteNoteEntityFromRepository(noteEntity: NoteEntity){
        Thread(Runnable {
            notesDao!!.deleteNote(noteEntity)
        }).start()
    }

    fun saveNoteToRepository(noteEntity: NoteEntity){
        Thread(Runnable {
            notesDao!!.insertNote(noteEntity)
        }).start()
    }

    fun saveNotesToRepository(noteEntities: List<NoteEntity>){
        Thread(Runnable {
            notesDao!!.insertAll(noteEntities)
        }).start()
    }

    fun closeDatabase(){
        notesDatabase?.close()
    }
}