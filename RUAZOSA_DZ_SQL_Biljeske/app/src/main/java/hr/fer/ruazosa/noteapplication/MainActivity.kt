package hr.fer.ruazosa.noteapplication

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), OnItemClickListener {

    private val EDIT_NOTE_ACTIVITY_REQUEST_CODE = 1
    private val NEW_NOTE_ACTIVITY_REQUEST_CODE = 2
    private var oldNoteEntity: NoteEntity? = null;
    lateinit var notesAdapter: NotesAdapter
    lateinit var viewModel: NotesViewModel

    override fun onItemClicked(noteEntity: NoteEntity) {
        //Toast.makeText(this,"Note was selected!\n Note title: ${note.noteTitle} \n", Toast.LENGTH_LONG).show()
        oldNoteEntity = noteEntity
        val intent = Intent(this, NotesDetails::class.java)
        // ovdje moze predati i object Note/NoteEntity ali tada Note treba biti Parceable
        // tj. implementirati to sucelje
        intent.putExtra("Request_Code", EDIT_NOTE_ACTIVITY_REQUEST_CODE)
        intent.putExtra("Note_Title", noteEntity.noteTitle)
        intent.putExtra("Note_Description", noteEntity.noteDescription)
        startActivityForResult(intent, EDIT_NOTE_ACTIVITY_REQUEST_CODE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listOfNotesView.layoutManager = LinearLayoutManager(applicationContext)

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.cell_divider)!!)
        listOfNotesView.addItemDecoration(decorator)

        viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)

        //prvo dohvati sve biljeske iz baze podataka
        viewModel.getNotesRepository()

        notesAdapter = NotesAdapter(viewModel, this)
        listOfNotesView.adapter = notesAdapter

        viewModel.notesList.observe(this, Observer {
            notesAdapter.notifyDataSetChanged()
        })

        newNoteActionButton.setOnClickListener {
            val intent = Intent(this, NotesDetails::class.java)
            intent.putExtra("Request_Code", NEW_NOTE_ACTIVITY_REQUEST_CODE)
            startActivityForResult(intent, NEW_NOTE_ACTIVITY_REQUEST_CODE)
        }
    }


    override fun onResume() {
        super.onResume()
        notesAdapter.notifyDataSetChanged()
        viewModel.getNotesRepository()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Check that it is the NotesDetails activity with an OK result
        if (requestCode == EDIT_NOTE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if(data != null){
                    // dohvati podatke o azuriranoj biljesci
                    val editNoteTitle = data!!.getStringExtra("Note_Title")
                    val editNoteDescription = data!!.getStringExtra("Note_Description")
                    var noteEntity: NoteEntity = NoteEntity(oldNoteEntity!!.noteId, noteTitle = editNoteTitle, noteDescription = editNoteDescription, noteDate = Date())
                    // azuriraj biljesku
                    if((oldNoteEntity!!.noteTitle != noteEntity.noteTitle) || (oldNoteEntity!!.noteDescription != noteEntity.noteDescription)){
                        if(oldNoteEntity != null) {
                            viewModel.updateNoteFromRepository(noteEntity)
                            viewModel.getNotesRepository()
                        }
                    }
                }
            }
        }else if(requestCode == NEW_NOTE_ACTIVITY_REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK) {
                if(data != null){
                    // dohvati podatke o novoj biljesci
                    val newNoteTitle = data!!.getStringExtra("Note_Title")
                    val newNoteDescription = data!!.getStringExtra("Note_Description")
                    var newNoteEntity: NoteEntity = NoteEntity(noteId = 0, noteTitle = newNoteTitle, noteDescription = newNoteDescription, noteDate = Date())
                    // spremi biljesku
                    viewModel.saveNoteToRepository(newNoteEntity)
                    viewModel.getNotesRepository()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.closeNotesRepository()
    }

}