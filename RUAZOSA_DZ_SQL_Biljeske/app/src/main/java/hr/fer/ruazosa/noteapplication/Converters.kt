package hr.fer.ruazosa.noteapplication

import android.arch.persistence.room.TypeConverter
import java.util.Date

// This code was taken from:
// https://developer.android.com/training/data-storage/room/referencing-data

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}